// Advanced Programming, A. Wąsowski, IT University of Copenhagen
//
// Group number: 14 
//
// AUTHOR1: Mikkel Agerlin (MAGC)
// TIME1: 6 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: Gustav Agergaard Winberg
// TIME2: 4 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR3: Jonleif Davidsen
// TIME2: ___ hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// This file is compiled with 'sbt compile' and tested with 'sbt test'.
//
// The file shall always compile and run after you are done with each exercise
// (if you do them in order).  Please compile and test frequently. Of course,
// some tests will be failing until you finish. Only hand in a solution that
// compiles and where tests pass for all parts that you finished.    The tests
// will fail for unfnished parts.  Comment such out.

package adpro

// Exercise  1

/* We create OrderedPoint as a trait instead of a class, so we can mix it into
 * Points (this allows to use java.awt.Point constructors without
 * reimplementing them). As constructors are not inherited, We would have to
 * reimplement them in the subclass, if classes not traits are used.  This is
 * not a problem if I mix in a trait construction time. */

trait OrderedPoint extends scala.math.Ordered[java.awt.Point] {

  this: java.awt.Point =>
  override def compare (that: java.awt.Point): Int =  
    if (that.x < this.x || (that.x == this.x && that.y < this.y)) {
      return 1
    } else {
      return -1
    } 
}

// Try the following (and similar) tests in the repl (sbt console):
// val p = new java.awt.Point(0,1) with OrderedPoint
// val q = new java.awt.Point(0,2) with OrderedPoint
// assert(p < q)

// Chapter 3


sealed trait Tree[+A]
case class Leaf[A] (value: A) extends Tree[A]
case class Branch[A] (left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  def size[A] (t :Tree[A]): Int = t match {
      case Leaf(_) => 1
      case Branch(left, right) => 1 + size(left) + size(right)
  }

  // Exercise 3 (3.26)

  def maximum (t: Tree[Int]): Int = t match {
      case Leaf(n) => n
      case Branch(left, right) => maximum(left) max maximum(right) 
  }

  // Exercise 4 (3.28)

  def map[A,B] (t: Tree[A]) (f: A => B): Tree[B] = t match {
    case Leaf(n) => Leaf(f(n)) 
    case Branch(left, right) => Branch(map(left) (f), map(right) (f))
  }

  // Exercise 5 (3.29)
  def fold[A,B] (t: Tree[A]) (f: (B,B) => B) (g: A => B): B = t match {
    case Leaf(n) =>  g(n)
    case Branch(left, right) => f(fold(left) (f) (g), (fold(right) (f) (g)))
  }

  def size1[A] (t: Tree[A]): Int = fold[A, Int](t) (_ + _ + 1) ((leaf: A) => 1)

  def maximum1[A] (t: Tree[Int]): Int = fold[Int, Int](t) (_ max _) ((n: Int) => n)

  def map1[A,B] (t: Tree[A]) (f: A=>B): Tree[B] = fold[A, Tree[B]](t) (Branch(_,_)) (n => Leaf(f(n)))
}

sealed trait Option[+A] {

  // Exercise 6 (4.1)
  def map[B] (f: A=>B): Option[B] = this match {
    case None => None
    case Some(n) => Some(f(n))
  }

  // You may Ignore the arrow in default's type below for the time being.
  // (it should work (almost) as if it was not there)
  // It prevents the argument "default" from being evaluated until it is needed.
  // So it is not evaluated in case of Some (the term is 'call-by-name' and we
  // should talk about this soon).

  def getOrElse[B >: A] (default: => B): B = this match {
      case None => default
      case Some(n) => n
  }

  def flatMap[B] (f: A=>Option[B]): Option[B] = this match {
    case None => None
    case Some(n) => (f(n))
  }

  def filter (p: A => Boolean): Option[A] = this match {
    case None => None
    case Some(n) => if (p(n)) Some(n) else None
  }
}

case class Some[+A] (get: A) extends Option[A]
case object None extends Option[Nothing]

object ExercisesOption {

  // Remember that mean is implemented in Chapter 4 of the text book

  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  // Exercise 7 (4.2)

  def variance (xs: Seq[Double]): Option[Double] = for{
    m <- mean(xs);
    variance <- mean(xs.map(el => math.pow((el - m), 2)))
  } yield variance

  // Exercise 8 (4.3)
  def map2[A,B,C] (ao: Option[A], bo: Option[B]) (f: (A,B) => C): Option[C] = for {
    a <- ao
    b <- bo
    res <- Some(f(a, b)) // flatMap implicitly fails if given any None, so we can wrap in Some if we go here
  } yield res

  // Exercise 9 (4.4) - This creates nested calls of Options of lists to map2, which are implicitly packed out by the implicit calls to flatMap in map2's for-comprehension - MAGIC!
  def sequence[A] (aos: List[Option[A]]): Option[List[A]] = aos.foldRight (Some(Nil): Option[List[A]]) ((x,y) => map2(x,y) (_::_))

  // Exercise 10 (4.5)
  def traverse[A,B] (as: List[A]) (f :A => Option[B]): Option[List[B]] = sequence(as.map(n => f(n)))
  // Slightly more efficient implementation, only 1 pass of the list. Harder to read though.
  def traverseHard[A, B] (as: List[A]) (f :A => Option[B]): Option[List[B]] = as.foldRight[Option[List[B]]] (Some(Nil): Option[List[B]]) ((x,y) => map2(f(x), y) (_::_))
}
